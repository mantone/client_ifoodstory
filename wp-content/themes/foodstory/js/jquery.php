<?php 
header('Content-type: text/javascript');   
header("Cache-Control: must-revalidate"); 
$offset = 72000 ; 
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT"; 
header($ExpStr);


//	global $shortname;
//	$settings = get_option($shortname.'_options');
//	 Variables should be added with {} brackets // {$settings['theme_option_color']}


fancytransition_jquery();
anythingslider_jquery();
orbit_jquery();
smoothdiv_jquery();
serialscroller_jquery();
jcycle_jquery();





function fancytransition_jquery() {
	//We have to get the Post ID, because the redirect does'nt pickup the post meta data
	
	$jqpostid = get_query_var('jqpostid');
	$csoptions = get_post_meta($jqpostid, THEMECUSTOMMETAKEY, true);	
	
	
	// ---------------------------	
	$delay = set_default_value( $csoptions["fancytransitions_delay"], 2500 );	
	$csoptions["fancytransitions_enablenextprev"] = checkbox_truefalse($csoptions["fancytransitions_enablenextprev"]);	
	if($csoptions["gallery_imagesize"] != "" ){	
		$ftwidth = get_option($csoptions["gallery_imagesize"].'_size_w');
		$ftheight = get_option($csoptions["gallery_imagesize"].'_size_h');	
	} else {
		$ftwidth = 540;
		$ftheight = 360;
	}

	
	$position = $csoptions["fancytransitions_position"];
	$direction = $csoptions["fancytransitions_direction"];	
	$navigation = checkbox_truefalse($csoptions["gallery_enablenextprev"]);		
	
	

	if($csoptions["gallery_type"] == "fancytransitions" ){		
print <<<END
	$(function(){
		
		$("#fancytransitions").jqFancyTransitions({
			effect: '{$csoptions["fancytransitions_effect"]}',	// wave, zipper, curtain
			width: {$ftwidth},															// width of panel
			height: {$ftheight},														// height of panel
			strips: 16,																		// number of strips
			delay: {$delay},																// delay between images in ms
			stripDelay: 20,																// delay beetwen strips in ms
			titleOpacity: 0.7,															// opacity of title
			titleSpeed: 1000, 															// speed of title appereance in ms
			position: '{$position}', 												// top, bottom, alternate, curtain
			direction: '{$direction}', 												// left, right, alternate, random, fountain, fountainAlternate
			navigation: {$navigation}, 											// prev and next navigation buttons
			links: false 																	// show images as links
		}); 	
	
		// NAVIGATION FADEIN ON HOVER
			$('#fancytransitions').hoverIntent(
				function() { $('.ft-next, .ft-prev').fadeIn(); },
				function() { $('.ft-next, .ft-prev').fadeOut(); }
			);
	

	});
END;

	}



}



function orbit_jquery() {
	
	//	WE HAVE TO GET THE POST ID, BECAUSE THE REDIRECT 
	//	DOESN'T PICKUP THE POST META DATA
	$jqpostid = get_query_var('jqpostid');
	$meta = get_post_meta($jqpostid, THEMECUSTOMMETAKEY, true);


	if($meta["gallery_type"] == "orbit" ){	
	
print <<<END
	$(function(){
	
	/*****************************
		ORBIT GALLERY SETTINGS
	*****************************/
		$('#orbit').orbit({
			'bullets': true,
			'timer' : true,
			'animation' : 'fade'
		});
	
	});
END;


	}


}



function anythingslider_jquery() {
	
	// WE HAVE TO GET THE POST ID, BECAUSE THE REDIRECT DOESN'T PICKUP THE POST META DATA
	$jqpostid = get_query_var('jqpostid');
	$meta = get_post_meta($jqpostid, THEMECUSTOMMETAKEY, true);	

	// ---------------------------		
	if($meta["gallery_imagesize"] != "" ){	
		$width = get_option($meta["gallery_imagesize"].'_size_w');
		$height = get_option($meta["gallery_imagesize"].'_size_h');	
	} else {
		$width = 540;
		$height = 360;
	}
	


	if($meta["gallery_type"] == "anythingslider" ){	
	
print <<<END
	$(function(){
		
		$('#slider').anythingSlider({
				width               	: {$width},      		// 	IF RESIZECONTENT IS FALSE, THIS IS THE DEFAULT WIDTH IF PANEL SIZE IS NOT DEFINED
				height              	: {$height},     		// 	IF RESIZECONTENT IS FALSE, THIS IS THE DEFAULT HEIGHT IF PANEL SIZE IS NOT DEFINED
				resizeContents  	: false,    				// 	IF TRUE, SOLITARY IMAGES/OBJECTS IN THE PANEL WILL EXPAND TO FIT THE VIEWPORT
				autoPlay            	: true,     				//		THIS TURNS OFF THE ENTIRE SLIDESHOW FUNCTIONALY, NOT JUST IF IT STARTS RUNNING OR NOT
				toggleControls     : false,
				navigationFormatter : formatText // Format navigation labels with text
		});	
		

		
		
	});
	
	function formatText(index, panel) {
		  return index + "";
	}	
	
END;

	}


}









function smoothdiv_jquery() {

	$jqpostid = get_query_var('jqpostid');		// WE HAVE TO GET THE POST ID, BECAUSE THE REDIRECT DOESN'T PICKUP THE POST META DATA
	$meta = get_post_meta($jqpostid, THEMECUSTOMMETAKEY, true);	
	
	if($meta["gallery_type"] == "smoothdiv" ){		
	
print <<<END
	$(function(){
	
	/*****************************
		SMOOTH DIV
	*****************************/
	$("div#makeMeScrollable").smoothDivScroll({
		scrollingHotSpotLeft:	"div.scrollingHotSpotLeft",				// The identifier for the hotspot that triggers scrolling left.
		scrollingHotSpotRight:	"div.scrollingHotSpotRight",		// The identifier for the hotspot that triggers scrolling right.
		scrollWrapper:	"div.scrollWrapper",					// The identifier of the wrapper element that surrounds the scrollable area.
		scrollableArea:	"div.scrollableArea",					// The identifier of the actual element that is scrolled left or right.	
		scrollingSpeed: 12, 
		mouseDownSpeedBooster: 3, 
		// autoScroll: "onstart", 
		autoScrollDirection: "endlessloop", 
		autoScrollSpeed: 2, 
		visibleHotSpots: "always", 
		hotSpotsVisibleTime: 9
		}
	);
	
	$('div#makeMeScrollable IMG').removeAttr("title");	
	$('div#makeMeScrollable IMG A').removeAttr("title");	


	});
END;

	}


}





function serialscroller_jquery() {

	$jqpostid = get_query_var('jqpostid');		// WE HAVE TO GET THE POST ID, BECAUSE THE REDIRECT DOESN'T PICKUP THE POST META DATA
	$meta = get_post_meta($jqpostid, THEMECUSTOMMETAKEY, true);	

	if($meta["gallery_type"] == "serialscroller" ){		

print <<<END
	$(function(){

		/*****************************
			SERIALSCROLLER
		*****************************/
		$('.serialscroller').each(function(i) {

			var targetid = $(this).attr('id');

			$('#'+targetid+' .scrollcontent').serialScroll({
				items:'div.slide_item',
				prev:'#'+targetid+' .scrollboxnav a.previtem',
				next:'#'+targetid+' .scrollboxnav a.nextitem',
				offset:0, 	//when scrolling to photo, stop 230 before reaching it (from the left)
				start:0, 	//as we are centering it, start at the 2nd
				duration: 900,
				force: true,
				stop: true,
				lock: false,
				cycle: false, // don't pull back once you reach the end
				easing:'easeInOutQuart', //use this easing equation for a funny effect
				jump: true //click on the images to scroll to them
			});

			
			var newheight = $(".slide_item", this).height();
			
			$('#'+targetid+' .slide_content').css({ top : -newheight+'px', height : newheight+'px'});		
			
			$('.scrollcontent .slide_item IMG', this).each(function(i) {	
				var imagewidth = $(this).width();	
				var imageheight = $(this).height();
				var ratio = newheight / imageheight;

				var newwidth = imagewidth * ratio;
				
				$(this).css("height", newheight + "px");				
				$(this).css("width", newwidth + "px");			
			});			
		
			$('#'+targetid).hoverIntent(
				function() { $('.scrollboxnav').fadeIn(); },
				function() { $('.scrollboxnav').fadeOut(); }  
			);

			$('#'+targetid+' .slide_item').hoverIntent(
				function() { $('.slide_content', this).fadeIn(); },
				function() { $('.slide_content', this).fadeOut(); }  
			);	
			
			var linkwidth = $('#'+targetid+' .scrollboxnav .nextitem').width();
			var scrollboxnavwidth = $('#'+targetid+' .scrollboxnav').width();
			var leftvalue = ( scrollboxnavwidth / 2 ) - linkwidth ;
			$('#'+targetid+' .scrollboxnav .nextitem').css( { left : leftvalue+'px' });
			$('#'+targetid+' .scrollboxnav .previtem').css( { left : -leftvalue+'px' });
		
		});		
	

	});
END;

	}


}





function jcycle_jquery() {

	$jqpostid = get_query_var('jqpostid');		// WE HAVE TO GET THE POST ID, BECAUSE THE REDIRECT DOESN'T PICKUP THE POST META DATA
	$meta = get_post_meta($jqpostid, THEMECUSTOMMETAKEY, true);	

	if($meta["gallery_type"] == "jcyclegallery" ){		

print <<<END
	

	
	
END;

	}


}







function checkbox_truefalse($input) {

	if($input == "" ){
		$output = "false";
	} else {
		$output = "true";
	}	

	return $output;
}

?>