$(function(){

	// ALO Newsletter
	$(".alo_easymail_widget").wrap('<div id="newsletter">');
	$("#newsletter").append('<div class="cancel">CLOSE</div>');
	$("#newsletter").after('<div id="signup"><div class="cancelsignup">SIGNUP FOR our NEWSLETTER</div></div>');

	$("div#newsletter").hide();
	$(".cancelsignup").click(function(){
		$(this).hide();
		$("#newsletter").show();
	});	
	$("#newsletter .cancel").click(function(){
		$("#newsletter").hide();
		$(".cancelsignup").show();
	});	
	
	
	$("div#mc_signup").hide();
	$(".widget_mailchimpsf_widget h3").click(function(){
		$("div#mc_signup").toggle();
	});


    $("#ns_widget_mailchimp-3 INPUT:not(.button)").val("email@address.com");
    swapValues = [];
    $("#ns_widget_mailchimp-3 INPUT:not(.button)").each(function(i){
        swapValues[i] = $(this).val();
        $(this).focus(function(){
            if ($(this).val() == swapValues[i]) {
                $(this).val("");
            }
        }).blur(function(){
            if ($.trim($(this).val()) == "") {
                $(this).val(swapValues[i]);
            }
        });
    });



    setInterval( "slideSwitch()", 4000 );

		
});


function slideSwitch() {
    var $active = $('#fadeslider IMG.active');

    if ( $active.length == 0 ) $active = $('#fadeslider IMG:last');

    var $next =  $active.next().length ? $active.next()
        : $('#fadeslider IMG:first');

    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}

	

