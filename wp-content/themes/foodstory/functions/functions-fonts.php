<?php 
/**************************************************************
 [01] LOAD CUFON FONTS
**************************************************************/
function load_cufonfonts() {
   # echo "\n<!-- Cufon Fonts -->";
   # echo "\n<script type='text/javascript' src='".get_stylesheet_directory_uri()."/fonts/museo.font.js'></script>";	
   # echo "\n<script type='text/javascript' src='".get_stylesheet_directory_uri()."/fonts/league_gothic.js'></script>";					
}


/**************************************************************
 [02] ENABLE FONTFACE USAGE
**************************************************************/
function load_fontface() {
	echo "\n<link rel='stylesheet' type='text/css' href='".get_stylesheet_directory_uri()."/fonts/fontface/museo/stylesheet.css'/>";
	#echo "\n<link rel='stylesheet' type='text/css' href='".get_stylesheet_directory_uri()."/fonts/fontface/carthogothic/stylesheet.css'/>";	
	#echo "\n<link rel='stylesheet' type='text/css' href='".get_stylesheet_directory_uri()."/fonts/fontface/leaguegothic/stylesheet.css'/>";		
	# echo "\n<link rel='stylesheet' type='text/css' href='".get_stylesheet_directory_uri()."/fonts/fontface/titillium/stylesheet.css'/>";
}
?>