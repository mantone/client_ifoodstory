<?php
/**************************************************************
 REGISTER CUSTOM MENUS
 http://codex.wordpress.org/Function_Reference/register_nav_menu
 
 CUSTOMIZE NAV MENUS ARE LOCATED IN THE BACK END OF WORDPRESS AT
 /wp-admin/nav-menus.php
 
 TO UTILIZE REGISTERED NAV MENUS PLEASE READ THE CODEX
 http://codex.wordpress.org/Function_Reference/wp_nav_menu
**************************************************************/
function register_theme_menu() {
	register_nav_menu( 'home-menu', __( 'Masthead Menu' ) );
	register_nav_menu( 'primary-menu', __( 'Primary Container Menu' ) );
	register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );
	
	# REGISTER A UNIQUE NAV MENU
	# WILL PRODUCE A LOCATION NAME
	$themefoldername = get_stylesheet();		# USE THE THEMES' FOLDER NAME AS THE PREFIX FOR THE MENU LOCATION NAME
	$location = $themefoldername."-menu";	# THE LOCATION NAME, USE THIS NAME WITH THE wp_nav_menu() function
	$location_displayname = __( ucwords( $themefoldername." Header Menu") );
	register_nav_menu( $location, $location_displayname );
}
?>