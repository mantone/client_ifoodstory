	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class('item'); ?>>	
				<div class="itemhead">	

					<?php edit_post_link('Edit','<div class="editlink"><span>','</span></div>'); ?>	
			
					<?php ####### [3] If A Catergory is of the name, "Features" give it a css class selector of the same name ?>
					<h2>
						<a href="<?php the_permalink() ?>" rel="bookmark" title='<?php printf(__('Permanent Link to "%s"',''), strip_tags(get_the_title())) ?>'><?php the_title(); ?></a>
					</h2>

					<?php ####### [4] Display - Author, Date, Time Since, Comments, and Number of Views ?>
					<div class="metadata">					
						<?php printf(__('<span class="author-meta">by %s </span>',''), '<a href="' .  get_author_posts_url( get_the_author_meta( 'ID' ) ).'">' . get_the_author() . '</a>'); ?>
						<span class="chronodata"> 					
							<?php echo get_the_time(__('F jS, Y','')); ?>
						</span>
						<?php comments_popup_link('<span class="commentsdata">'.__('0 Comments','').'</span>', '<span class="commentsdata">'.__('1 Comment','').'</span>', '<span class="commentsdata">% '.__('Comments','').'</span>', 'commentslink', '<span class="commentslink">'.__('Closed','').'</span>'); ?>						
						<br />            
						<span class="categorydata">Category: <?php the_nice_category(', ', ' &amp; '); ?></span>
						
						<br /> 
						<?php the_tags('<span class="tagdata">Tags: ', ', ', '</span>'); ?>
					</div>
				</div>

				<div class="itemtext">		
					<?php
							show_mediagalleries();
							the_content("Continue reading...");
					?>
				</div>

				
			</div>
					
		<?php endwhile; ?>		

	<?php else : ?>
			<div class="item">
					<div class="itemhead">	
						<h3 class="center">Nothiing here to see folks! :-(</h3>
					</div>

					<div class="itemtext">
						<p>Perhaps you can try a different search term</p>
					</div>
			</div>
	<?php endif; ?>