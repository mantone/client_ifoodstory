<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
<body <?php body_id(); ?> <?php body_class(); ?>>
<div id="wrapper">
    <?php get_template_part( 'slide', 'top'); ?>

    <div id="page"><div id="index">

        <!-- Masthead -->
        <?php get_template_part( 'masthead'); ?>

        <?php get_template_part( "eventbanner"); ?>

        <!-- Content is King -->
        <div id="content">

            <?php dynamicsidebar( 'Content Featured', '<div id="content-featured">', '</div>'); ?>

            <div id="primary">

                <div id="giftcertificate">
                    <a href="/gift-certificates" style="position: absolute; width: 460px; height: 66px;"></a>
                    <div>
                        <h2 style="color: #66605f; font-size: 22px; float: left; width: 200px; line-height: 32px;">
                            Gift Certificates?
                        </h2>
                        <div style="float: right; width: 228px; text-align: center;">
                            “We offer a public cooking workshop
                            several times per month.”
                        </div>
                    </div>
                </div>

                <?php dynamicsidebar( "Primary Featured", '<div id="primary-featured">', '</div>' ); ?>



                <div id="postbox">
                    <div id="fadeslider">
                            <img class="active" width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/1-food1-460x460.jpg" class="attachment-medium" alt="1-food1"
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/2-Lecture1-460x460.jpg" class="attachment-medium" alt="2-Lecture1">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/2A-presentation2-460x460.jpg" class="attachment-medium" alt="2A-presentation2">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/3-IMG_7586-460x460.jpg" class="attachment-medium" alt="3-IMG_7586">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/3A-460x460.jpg" class="attachment-medium" alt="3A">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/9C-mame-460x460.jpg" class="attachment-medium" alt="9C-mame">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/9F-460x460.jpg" class="attachment-medium" alt="9F">
                            <img width="460" height="459" src="http://ifoodstory.com/wp-content/uploads/2010/05/4-IMG_9761-460x459.jpg" class="attachment-medium" alt="4-IMG_9761">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/9G-460x460.jpg" class="attachment-medium" alt="9G">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/9AUdon-460x460.jpg" class="attachment-medium" alt="Making Udon!">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/6-IMG_9869-460x460.jpg" class="attachment-medium" alt="6-IMG_9869">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/8-460x460.jpg" class="attachment-medium" alt="8">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/9BTofu-460x460.jpg" class="attachment-medium" alt="9BTofu">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/9D-kuzumanjyu-460x460.jpg" class="attachment-medium" alt="9D-kuzumanjyu">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/FoodStory_tofu-1060925-460x460.jpg" class="attachment-medium" alt="FoodStory_tofu-1060925">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/IMG_7669-460x460.jpg" class="attachment-medium" alt="IMG_7669">
                            <img width="460" height="460" src="http://ifoodstory.com/wp-content/uploads/2010/05/IMG_96611-460x460.jpg" class="attachment-medium" alt="IMG_9661">
                    </div>
                </div>



                <?php get_template_part( 'navigate'); ?>

            </div>

            <?php get_template_part( 'secondary'); ?>

        </div>

    </div></div>

    <!-- Footer -->
    <?php get_footer(); ?>

    <?php get_template_part( 'slide', 'bottom'); ?>
</div>
<?php wp_footer(); ?>
</body>
</html>
