<?php
/**************************************************************
 WELCOME TO THE FOUNDATION CHILD THEME TEMPLATE
 
 THIS TEMPLATE WAS CREATED TO HELP FACILITATE THE
 CREATION OF CHILD THEMES FOR theFOUNDATION
 THEME FRAMEWORK
 
 FOR INFORMATION ON HOW FILES ARE ORAGINIZED
 AND NAMING CONVENTIONS OF FUNCTIONS PLEASE
 VISIT
 
 http://thefoundationframework.com/organize
**************************************************************/


/**************************************************************
 [00] CUSTOM POST TYPES CUSTOM POST TYPES LOADED FROM PARENT THEME
 NOTE: SHOULD BE ENABLED IN THE CHILD THEME
 
 IF YOU ARE INTERESTED IN DEVELOPMING CUSTOM POST TYPES FOR THIS 
 FRAMEWORK PLEASE CONTACT...
 
 NOTE:
 TEMPLATEPATH -> PARENT THEME
 STYLESHEETPATH -> CHILD THEME
**************************************************************/
#require_once(TEMPLATEPATH . '/functions/functions-posttype-event.php');
#require_once(TEMPLATEPATH . '/functions/functions-posttype-dictionary.php');
#require_once(STYLESHEETPATH . '/functions/functions-posttype-portfolio.php');
#require_once(TEMPLATEPATH . '/functions/functions-posttype-designer.php');
#require_once(TEMPLATEPATH . '/functions/functions-posttype-swatch.php');
#require_once(TEMPLATEPATH . '/functions/functions-posttype-product.php');


/**************************************************************
 [01] CHILD THEME APPEARANCE OPTION OVERRIDES
**************************************************************/
require_once(STYLESHEETPATH . '/functions/functions-appearance-sidebars.php');
require_once(STYLESHEETPATH . '/functions/functions-appearance-header.php');
require_once(STYLESHEETPATH . '/functions/functions-appearance-widgets.php');
require_once(STYLESHEETPATH . '/functions/functions-appearance-menu.php');


/**************************************************************
 [02] CHILD THEME FONT FUNCTIONS OVERRIDE
**************************************************************/
require_once(STYLESHEETPATH . '/functions/functions-fonts.php');

/**************************************************************
 [03] CHILDTHEME JQUERY + JQUERY LIBRARIES OVERRIDES
**************************************************************/
require_once(STYLESHEETPATH . '/functions/functions-jquery.php');

/**************************************************************
 [04] OVERRIDES FOR 3rd PARTY PLUGINS
**************************************************************/
require_once(STYLESHEETPATH . '/functions/functions-override-plugin.php');

/**************************************************************
 [05] REMOVE ADMIN BAR
**************************************************************/
#add_filter( 'show_admin_bar', '__return_false' );
#wp_deregister_script('admin-bar');
#wp_deregister_style('admin-bar');
#remove_action('wp_footer','wp_admin_bar_render',1000);	
#add_action( 'admin_print_scripts-profile.php', 'hide_admin_bar_prefs' );
function hide_admin_bar_prefs() { 
	echo '
	<style type="text/css">
		.show-admin-bar { display: none; }
	</style>
	';
}



/**************************************************************
 [06] IMAGE FORMATS
**************************************************************/
function setup_theme_image_formats() {	
		
		global $content_width;
		
		$content_width = 460;

		if(is_page())
			$content_width = 880;
		
		
		if (is_admin() && isset($_GET['activated'] ) && $pagenow == "themes.php" ) {
			update_option( 'thumbnail_size_w',  '165' );
			update_option( 'thumbnail_size_h', '110' );
			update_option( 'thumbnail_crop', 1 );
			
			update_option( 'medium_size_w', '460' );
			update_option( 'medium_size_h', '560' );
			
			update_option( 'large_size_w', '940' );
			update_option( 'large_size_h', '780' );	
						
			#header( 'Location: '.admin_url().'admin.php?page=my_theme' ) ;
		}		


		

	#	SETUP DEFAULT CROP OPTIONS ON DEFAULT WORDPRESS MEDIA SIZES
		if(	false === get_option("medium_crop")	)
			add_option("medium_crop", "0");
		else
			update_option("medium_crop", "0");
			
		if(false === get_option("large_crop"))
			add_option("medium_crop", "0");
		else
			update_option("large_crop", "0");			
	
	#	ESTABLISH CUSTOM THUMBSIZE SIZES	
		add_image_size( "minithumbnail", 85, 50, true );		// DIMENSION SIZE FOR THUMBNAIL SIZE 	:: 360 X 240
		add_image_size( "minimedium", 130, 75, true );			// DIMENSION SIZE FOR MEDIUM 			:: 540 X 360
		add_image_size( "minilarge", 170, 95, true );				// DIMENSION SIZE FOR LARGE IMAGES 		:: 880 X 540
		add_image_size( "headerlogo", 180, 180, true );			// DIMENSION SIZE FOR HEADER IMAGES
		add_image_size( "squarethumbnail", 50, 50, true );		// DIMENSION SIZE FOR LARGE IMAGES 		:: 880 X 540
		add_image_size( "squaremedium", 80, 80, true );		// DIMENSION SIZE FOR LARGE IMAGES 		:: 880 X 540
		add_image_size( "squarelarge", 160, 160, true );			// DIMENSION SIZE FOR LARGE IMAGES 		:: 880 X 540		
}

/**************************************************************
 [07] ENABBLE SHORT CODE AND AUTOEMBED FOR WIDGETS
**************************************************************/
add_filter( 'widget_text', array( $wp_embed, 'run_shortcode' ), 8 );
add_filter( 'widget_text', array( $wp_embed, 'autoembed'), 8 );


/************************************************************************
 [09] REPLACE EXCERPT ELLIPSE
************************************************************************/
function replace_excerpt($content) {
	return str_replace('[...]', '...<a class="read_more" href="'. get_permalink() .'">Continue Reading &raquo;</a>', $content);
}
add_filter('get_the_excerpt', 'replace_excerpt', 19);


?>

