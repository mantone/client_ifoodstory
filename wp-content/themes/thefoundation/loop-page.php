	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class('item'); ?>>	

				<?php get_template_part( 'itemhead', 'page'); ?>			

				<div class="itemtext">
					<?php 
							global $content_width;
							$content_width = 940;					
									
							show_mediagalleries();
							the_content("Continue reading...");				
					?>			
				</div>
			</div>
					
		<?php endwhile; ?>		

		<!-- NAVIGATION -->
		<?php get_template_part( 'navigate', 'page' ); ?>

	<?php else : ?>
			<div class="item">
				<?php get_template_part( 'nothing' ); ?>
			</div>
	<?php endif; ?>