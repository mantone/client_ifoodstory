<?php
/****
 * functions.php
 *
 * STORES FRAMEWORK FUNCTIONS
 */

/**
 * SETUP UNIVERSAL CONSTANTS IF THEY HAVE NOT BEEN SET BY CHILD THEMES
 */
define('TEXTDOMAIN', 'thefdt');
define('THEMECUSTOMMETAKEY', '_fsl_media_options');
if (!isset($content_width)) $content_width = get_option('medium_size_w'); // SHOULD BE OVER WRITTEN IN CHILD THEMES

/**
 * DEPRECATED FUNCTIONS
 *
 * @deprecated
 */
locate_template('functions/functions-deprecated.php', true);


/**
 * HELPER FUNCTIONS
 *
 * @deprecated
 */
locate_template('functions/functions-helpers.php', true);

/**
 * HELPER FUNCTIONS
 *
 * @deprecated
 */
locate_template('functions/functions-shortcodes.php', true);

/**
 * SETUP THEME OPTIONS AND ADDITIONAL APPEARANCE OPTIONS
 */
locate_template('functions/functions-appearance-themeoptions.php', true);
locate_template('functions/functions-appearance-sidebars.php', true);
locate_template('functions/functions-appearance-widgets.php', true);
locate_template('functions/functions-appearance-menu.php', true);
if (of_get_option('enable_wordpress_background', false))
    locate_template('functions/functions-appearance-background.php', true);
if (of_get_option('enable_wordpress_header', false))
    locate_template('functions/functions-appearance-header.php', true);

/**
 * SANDBOX FUNCTIONS
 */
locate_template('functions/functions-sandbox.php', true);

/**
 * Parent Functions Hook
 */
do_action( 'parent_functions_file' );

/**
 * SOCIAL MEDIA FUNCTIONS
 */
locate_template('functions/functions-social-media.php', true);

/**
 * ENQUEUE JQUERY + JQUERY LIBRARIES
 */
locate_template('functions/functions-jquery.php', true);

/**
 * ENABLE DYNAMIC GENERATED JS + CSS
 */
locate_template('functions/functions-dynamic-js.php', true);
locate_template('functions/functions-dynamic-css.php', true);

/**
 * MEDIA FUNCTIONS
 */
locate_template('functions/functions-media-galleries.php', true);

/**
 * REMOVE WORDPRESS DEFAULT FEATURES
 */
locate_template('functions/functions-remove-features.php', true);

/**
 * ADMIN DASHBOARD SETTINGS
 */
locate_template('functions/functions-dashboard.php', true);

/**
 * ADMIN PAGE/POST/MEDIA EDIT COLUMNS
 */
locate_template('functions/functions-appearance-admincolumns.php', true);

/**
 * ADMIN BAR
 */
locate_template('functions/functions-adminbar.php', true);

/**
 * ADD_THEME_SUPPORT
 */
locate_template('functions/functions-theme-support.php', true);



/**
 * SETTINGS FOR MEDIA SIZES
 */
locate_template('functions/functions-settings-media.php', true);
if (function_exists('thefdt_settings_media')) {
    thefdt_settings_media();
}


/**
 * OVERRIDES FOR 3rd PARTY PLUGINS
 */
locate_template('functions/functions-override-plugin.php', true);


/**
 * INTIATE FRAMEWORK JQUERY SETUP
 *
 * @TODO MOVE TO DEPRECATED
 */
if (function_exists('init_jquery')) {
    init_jquery();
}


/**
 * REGISTER SIDEBARS
 *
 * @TODO MOVE TO DEPRECATED
 */
if (function_exists('framework_register_sidebars')) {
    framework_register_sidebars();
}

/**
 * REGISTER CUSTOM WORDPRESS MENU
 *
 * @PLUGGABLE
 *
 * @TODO: MOVE TO DEPRECATED
 */
if (function_exists('register_theme_menu')) {
    add_action('init', 'register_theme_menu');
}


?>