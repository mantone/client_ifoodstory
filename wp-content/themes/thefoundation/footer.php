<footer id="footer">
	<div class="box">
		<?php
		wp_nav_menu( 
			array( 
				'theme_location' => 'footertop-nav',			# DISPLAYS A CUSTOM SELECTED MENU FOR THE SPECIFICED LOCATION, SHOW EMPTY IF NOT IS SELECTED
				'container' => 'div',							# CONTAINER TAG, NOT APPLIED WHEN 'fallback_cb' FUNCTION IS USED
				'container_id' => 'footertop',					# CONTAINTER ID, NOT APPLIED WHEN 'fallback_cb' FUNCTION IS USED
				'container_class' => '',						# CONTAINTER CLASS, NOT APPLIED WHEN 'fallback_cb' FUNCTION IS USED				
				'menu_id' => 'footertop-nav',						# UL ID			
				'menu_class' => 'footer-menu',					# UL CLASS
				'fallback_cb'  => null,		# DOES NOT WORK WITH THE 'theme_location' parameter
				'depth' => 0										# DEPTH OF MENU, '0' VALUE MEANS ALL
			) 
		); 
		?>

        <div class="panels">
            <div>
                <?php show_dynamic_sidebar("Footer Column 1", '<div class="col1">', '</div>'); ?>
                <?php show_dynamic_sidebar("Footer Column 2", '<div class="col2">', '</div>'); ?>
                <?php show_dynamic_sidebar("Footer Column 3", '<div class="col3">', '</div>'); ?>
            </div>
        </div>

        <div class="bar">
            <div>
                <?php show_dynamic_sidebar("Footer Column 4", '<div class="col4">', '</div>'); ?>
            </div>
        </div>

		<?php
		wp_nav_menu( 
			array( 
				'theme_location' => 'footerbottom-nav',				# DISPLAYS A CUSTOM SELECTED MENU FOR THE SPECIFICED LOCATION, SHOW EMPTY IF NOT IS SELECTED
				'container' => 'div',							# CONTAINER TAG, NOT APPLIED WHEN 'fallback_cb' FUNCTION IS USED
				'container_id' => 'footerbottom',					# CONTAINTER ID, NOT APPLIED WHEN 'fallback_cb' FUNCTION IS USED
				'container_class' => '',						# CONTAINTER CLASS, NOT APPLIED WHEN 'fallback_cb' FUNCTION IS USED
				'menu_id' => 'footerbottom-nav',						# UL ID			
				'menu_class' => 'footer-menu',					# UL CLASS
				'fallback_cb'  => null,		# DOES NOT WORK WITH THE 'theme_location' parameter
				'depth' => 0										# DEPTH OF MENU, '0' VALUE MEANS ALL
			) 
		); 
		?>
    </div>
</footer>