	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class('item'); ?>>

				<div class="itemhead">	
					<?php edit_post_link('Edit','<div class="editlink"><span>','</span></div>'); ?>				
					<h2>
						<a href="<?php the_permalink() ?>" rel="bookmark" title='<?php printf(__('Permanent Link to "%s"',''), strip_tags(get_the_title())) ?>'><?php the_title(); ?></a>
					</h2>

					<div class="metadata">			
						<?php printf(__('<span class="author-meta">by %s </span>', 'thefoundation' ), '<a href="' . get_author_posts_url(get_the_author_meta( 'ID' )) . '">' . get_the_author() . '</a>' ); ?>			

						<span class="chrono-meta"> 					
							<?php echo get_the_time(__('F jS, Y','')); ?>
						</span>
						<?php comments_popup_link('<span class="no-comments-meta">'.__('0 Comments','').'</span>', '<span class="one-comments-meta">'.__('1 Comment','').'</span>', '<span class="many-comments-meta">% '.__('Comments','').'</span>', 'comments-meta', '<span class="closed-comments-meta">'.__(' Comments Closed','').'</span>'); ?>						

						<span class="category-meta">
							Category: <?php the_nice_category(', ', ' &amp; '); ?>
						</span>
							
						<?php the_tags('<span class="tags-meta">Tags: ', ', ', '</span>'); ?>
					</div>
			
				</div>

				<div class="itemtext">
					<?php
						show_mediagalleries();
						the_content("Continue reading...");
					?>
				</div>

				
			</div>
					
		<?php endwhile; ?>		

	<?php else : ?>
			<div class="item">
			
				<?php get_template_part( 'nothing' ); ?>

			</div>
	<?php endif; ?>