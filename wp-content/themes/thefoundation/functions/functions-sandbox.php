<?php
/****
 * functions/functions-sandbox.php
 *
 * CUSTOM POST TYPE FOR EVENTS
 *
 * Having a file to hastily add or edit functions can be helpful.
 * Especially if you are writing new code or pasting in
 * snippets you have found handy.  It's good to try it out
 * here, and if it works out then think about formatting
 * the code based on code style guidelines.
 *
 */


/**
 * SubScript and SupScript Buttons
 *
 * @REF ttp://wpsnipp.com/index.php/functions-php/add-subscript-and-supscript-buttons-to-editor/
 */
function enable_more_buttons($buttons)
{
    $buttons[] = 'sub';
    $buttons[] = 'sup';
    return $buttons;
}

add_filter("mce_buttons_3", "enable_more_buttons");

/**
 * ADD DROPDOWN BOX FOR SHORTCODES
 *
 * @REF http://wpsnipp.com/index.php/functions-php/add-custom-media_buttons-for-shortcode-selection/
 */
#add_action('media_buttons','add_sc_select',11);
function add_sc_select()
{
    global $shortcode_tags;
    /* ------------------------------------- */
    /* enter names of shortcode to exclude bellow */
    /* ------------------------------------- */
    $exclude = array("wp_caption", "embed");
    echo '&nbsp;<select id="sc_select"><option>Shortcode</option>';
    foreach ($shortcode_tags as $key => $val) {
        if (!in_array($key, $exclude)) {
            $shortcodes_list .= '<option value="[' . $key . '][/' . $key . ']">' . $key . '</option>';
        }
    }
    echo $shortcodes_list;
    echo '</select>';
}

#add_action('admin_head', 'button_js');
function button_js()
{
    echo '<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery("#sc_select").change(function() {
			  send_to_editor(jQuery("#sc_select :selected").val());
        		  return false;
		});
	});
	</script>';
}

/**
 * GOOGLE DOC SHORT CODE VIEWER
 *
 * @REF http://wpsnipp.com/index.php/functions-php/new-google-docs-shortcode-for-psd-ai-svg-and-more/
 *
 * @param $atts
 * @param null $content
 * @return string
 */
function wps_viewer($atts, $content = null)
{
    extract(shortcode_atts(array(
        "href" => 'http://',
        "class" => ''
    ), $atts));
    return '<a href="http://docs.google.com/viewer?url=' . $href . '" class="' . $class . ' icon">' . $content . '</a>';
}

add_shortcode("doc", "wps_viewer");


/**
 * Move Author Meta Box into Publish Meta Box
 *
 * @ref http://wpsnipp.com/index.php/functions-php/move-author-metabox-options-to-publish-post-metabox/
 */
add_action('admin_menu', 'remove_author_metabox');
add_action('post_submitbox_misc_actions', 'move_author_to_publish_metabox');
function remove_author_metabox()
{
    remove_meta_box('authordiv', 'post', 'normal');
}

function move_author_to_publish_metabox()
{
    global $post_ID;
    $post = get_post($post_ID);
    echo '<div id="author" class="misc-pub-section" style="border-top-style:solid; border-top-width:1px; border-top-color:#EEEEEE; border-bottom-width:0px;">Author: ';
    post_author_meta_box($post);
    echo '</div>';
}


/**
 * Redirect Category Template to Single Post Template if only one entry
 *
 * @Ref http://wpsnipp.com/index.php/cat/redirect-to-a-single-post-if-one-post-in-category-or-tag/
 * @return void
 */
function redirect_to_post()
{
    global $wp_query;
    if (is_archive() && $wp_query->post_count == 1) {
        the_post();
        $post_url = get_permalink();
        wp_redirect($post_url);
    }
}

#add_action('template_redirect', 'redirect_to_post');


/**
 * ADD NO FOLLOW TO THE CONTENT AND EXCERPT
 *
 * @LINK http://wpsnipp.com/index.php/functions-php/nofollow-external-links-only-the_content-and-the_excerpt/
 */
function my_nofollow($content)
{
    return preg_replace_callback('/<a[^>]+/', 'my_nofollow_callback', $content);
}

function my_nofollow_callback($matches)
{
    $link = $matches[0];
    $site_link = get_bloginfo('url');
    if (strpos($link, 'rel') === false) {
        $link = preg_replace("%(href=\S(?!$site_link))%i", 'rel="nofollow" $1', $link);
    } elseif (preg_match("%href=\S(?!$site_link)%i", $link)) {
        $link = preg_replace('/rel=\S(?!nofollow)\S*/i', 'rel="nofollow"', $link);
    }
    return $link;
}

#add_filter('the_content', 'my_nofollow');
#add_filter('the_excerpt', 'my_nofollow');

/**
 * HOME MENU FALL BACK
 *
 * @LINK http://www.wprecipes.com/how-to-show-the-home-link-on-wp_nav_menu-default-fallback
 *
 * @param $args
 * @return
 */
function homelink_for_menufallback($args)
{
    $args['show_home'] = true;
    return $args;
}

add_filter('wp_page_menu_args', 'homelink_for_menufallback');


/**
 * ADD SEARCH BOX TO ADMIN BAR
 *
 * @REF http://www.wprecipes.com/how-to-automatically-add-a-search-field-to-your-navigation-menu
 *
 * @param $items
 * @param $args
 * @return string
 */
function add_search_box($items, $args)
{
    $searchform = get_search_form(false);
    $items .= '<li>' . $searchform . '</li>';

    return $items;
}

#add_filter('wp_nav_menu_items','add_search_box', 10, 2);


/**
 * ADD BROWSER DETECTION TO BODY CLASS FUNCTION
 *
 * @REF http://www.nathanrice.net/blog/browser-detection-and-the-body_class-function/
 */
function browser_body_class($classes)
{
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

    if ($is_lynx) $classes[] = 'lynx';
    elseif ($is_gecko) $classes[] = 'gecko';
    elseif ($is_opera) $classes[] = 'opera';
    elseif ($is_NS4) $classes[] = 'ns4';
    elseif ($is_safari) $classes[] = 'safari';
    elseif ($is_chrome) $classes[] = 'chrome';
    elseif ($is_IE) $classes[] = 'ie';
    else $classes[] = 'unknown';

    if ($is_iphone) $classes[] = 'iphone';
    return $classes;
}

//	add_filter('body_class','browser_body_class');


/**
 * CUSTOM TEMPLATE TAG FOR SIDEBARS
 *
 * @LINK http://codex.wordpress.org/Function_Reference/get_template_part
 *
 * @param $sidebar_name
 * @param string $before
 * @param string $after
 */
function dynamicsidebar($sidebar_name, $before = '', $after = '')
{
    if (function_exists('dynamic_sidebar')) {
        if(is_active_sidebar($sidebar_name)) {
            echo $before;
            dynamic_sidebar($sidebar_name);
            echo $after;
        }
    }

}


/***********************************************************
MOST WORDPRESS USERS ARE USING CUSTOM FIELDS TO DISPLAY
THUMBS ON THEIR BLOG HOMEPAGE. IT IS A GOOD IDEA, BUT DO
YOU KNOW THAT WITH A SIMPLE PHP FUNCTION, YOU CAN GRAB
THE FIRST IMAGE FROM THE POST, AND DISPLAY IT.
 ***********************************************************/
function find_img_inside_postcontent()
{
    global $post, $posts;

    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];

    if (empty($first_img)) { //Defines a default image
        $first_img = "/images/default.jpg";
    }
    return $first_img;
}


/************************************************************************
FUNCTIONS FOR IMAGE PREVIOUS AND IMAGE NEXT
MAKE SURE IT FACTORS IN THE FIRST AND LAST IMAGE
 *************************************************************************/
function ps_previous_image_link($f)
{
    $i = ps_adjacent_image_link(true);
    if ($i) {
        echo str_replace("%link", $i, $f);
    }
}

function ps_next_image_link($f)
{
    $i = ps_adjacent_image_link(false);
    if ($i) {
        echo str_replace("%link", $i, $f);
    }
}

function ps_adjacent_image_link($prev = true)
{
    global $post;
    $post = get_post($post);
    $attachments = array_values(get_children(Array('post_parent' => $post->post_parent,
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => 'ASC',
        'orderby' => 'menu_order ASC, ID ASC')));

    foreach ($attachments as $k => $attachment) {
        if ($attachment->ID == $post->ID) {
            break;
        }
    }

    $k = $prev ? $k - 1 : $k + 1;

    if (isset($attachments[$k])) {
        return wp_get_attachment_link($attachments[$k]->ID, 'thumbnail', true);
    }
    else {
        return false;
    }
}


/**************************************************************
[06] ADD CUSTOM POST TYPES TO THE 'RIGHT NOW' DASHBOARD WIDGET
http://wordpress.stackexchange.com/questions/1567/best-collection-of-code-for-your-functions-php-file
 **************************************************************/
function all_settings_link()
{
    add_theme_page(__('All Settings'), __('All Settings'), 'administrator', 'options.php');
}

add_action('admin_menu', 'all_settings_link');


/**************************************************************
CONSTRUCT XHTML TAGS USING A FUNCTION, $ARS IS A QUERY STRING
THAT WILL BE EXTRACTED.
 **************************************************************/
function itag($tag = "img", $content, $args = null, $precontent = "", $postcontent = "")
{

    if ($tag == "img" && $content != "") {

        $defaults = array(
            'id' => "",
            'class' => "",
            'title' => "",
            'alt' => "",
            'src' => ""
        );
        $args = wp_parse_args($args, $defaults);
        extract($args, EXTR_SKIP);

        $divxhmtl = "";

        if ($id != "") :
            $id = ' id="' . $id . '"';
        endif;

        if ($class != "") :
            $class = ' class="' . $class . '"';
        endif;

        if ($alt != "") :
            $alt = ' alt="' . $alt . '"';
        endif;

        if ($content != "") :
            $src = ' src="' . $content . '"';
        endif;

        $divxhtml = "\n" . '<' . $tag . $id . $class . $src . $alt . ' />';

        return $divxhtml;
    } else {
        return $content;
    }
}


function socialmedia_profiles()
{

    $profiles = array(

        array(
            'name' => "Facebook",
            'href' => "http://www.facebook.com/pages/Los-Angeles-CA/FOODSTORY/121934961176624",
            'hrefclass' => "facebook",
            'hreftitle' => "Join Our Facebook Page",
            'iconfolder' => "/images/followicons/",
            'iconfilename' => "facebook",
            'iconimgtype' => "png"),
        array(
            'name' => "Twitter",
            'href' => "http://www.twitter.com/foodstory_japan",
            'hrefclass' => "twitter",
            'hreftitle' => "Follow Us on Twitter",
            'iconfolder' => "/images/followicons/",
            'iconfilename' => "twitter",
            'iconimgtype' => "png"),
        array(
            'name' => "Youtube",
            'href' => "http://www.youtube.com/dailyFOODSTORY",
            'hrefclass' => "youtube",
            'hreftitle' => "Join our Youtube Channel",
            'iconfolder' => "/images/followicons/",
            'iconfilename' => "youtube",
            'iconimgtype' => "png")
    );

    echo get_socialmedia($profiles);

}

function get_socialmedia($profiles = null)
{

    if ($profiles) {
        $smedia .= "";

        foreach ($profiles as $key) {
            $smedia .= get_socialprofile($key);
        }

        $smedia = xtag("div", $smedia, "id=followme");
        $smedia = xtag("div", $smedia, "id=socialmedia");

        return $smedia;
    }
}


function get_socialprofile($args = null)
{

    $defaults = array(
        'name' => "",
        'href' => "",
        'hrefclass' => "",
        'hreftitle' => "",
        'iconfolder' => "/images/followicons/",
        'iconfilename' => "",
        'iconimgtype' => "png"
    );
    $args = wp_parse_args($args, $defaults);
    extract($args, EXTR_SKIP);


    $imgsrc = get_stylesheet_directory_uri() . $iconfolder . $iconfilename . "." . $iconimgtype;
    $hrefcontent = itag("img", $imgsrc, "alt=" . $hreftitle) . xtag("span", $name);
    $link = xtag("a", $hrefcontent, 'href=' . $href . '&class=' . $hrefclass . '&title=' . $hreftitle);
    $profile = xtag("div", $link, "class=socialprofile");


    $socialprofile = '
		<div class="socialprofile">
			<a class="' . $hrefclass . '" href="' . $href . '" title="' . $hreftitle . '">
			<img ' . $imgsrc . '/><span>' . $name . '</span></a>
		</div>
	';

    return $profile;

}


/**
 *
 * THE EXISTING THE_CATEGORY() FUNCTION HAS NO WAY OF TELLING HOW MANY
 * CATEGORIES THERE ARE, AND SO CAN'T DO SOMETHING FANCY LIKE INSERTING THE
 * WORD "AND" BETWEEN THE PENULTIMATE (SECOND-TO-LAST) AND ULTIMATE CATEGORIES.
 * EXAMPLE:
 * SINGLE CATEGORY: CATEGORY1
 * TWO CATEGORIES: CATEGORY1 AND CATEGORY 2
 * THREE CATEGORIES: CATEGORY1, CATEGORY2 AND CATEGORY3
 * FOUR CATEGORIES: CATEGORY 1, CATEGORY 2, CATEGORY 3 AND CATEGORY 4
 *
 * @href http://txfx.net/2004/07/22/wordpress-conversational-categories/
 * @param string $normal_separator
 * @param string $penultimate_separator
 */
function the_nice_category($normal_separator = ', ', $penultimate_separator = ' and ')
{
    echo  get_the_nice_category($normal_sperator, $penultimate_separator);
}

function get_the_nice_category($normal_separator = ', ', $penultimate_separator = ' and ')
{
    $categories = get_the_category();

    if (empty($categories)) {
        _e('Uncategorized');
        return;
    }

    $thelist = '';
    $i = 1;
    $n = count($categories);
    foreach ($categories as $category) {
        $category->cat_name = $category->cat_name;
        if (1 < $i && $i != $n) $thelist .= $normal_separator;
        if (1 < $i && $i == $n) $thelist .= $penultimate_separator;
        $thelist .= '<a href="' . get_category_link($category->cat_ID) . '" title="' . sprintf(__("View all posts in %s"), $category->cat_name) . '">' . $category->cat_name . '</a>';
        ++$i;
    }
    return apply_filters('the_category', $thelist, $normal_separator);
}


/**
 * FDT HELPER FUNCTION
 * RUN wp_enqueue_script WHEN $check is TRUE
 *
 * @param $scriptname
 * @param bool $check
 * @TODO: MOVE TO BETTER LOCATION
 */
function use_wp_enqueue($scriptname, $check = false)
{
    if ($check)
        wp_enqueue_script($scriptname);
}


/**
 * NICER EXCERPTS FOR THEME
 *
 * @param $text
 * @return string
 */
function improved_trim_excerpt($text)
{
    global $post;
    if ('' == $text) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);
        $text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
        $text = strip_tags($text, '<a><p><img>');
        $excerpt_length = 50;
        $words = explode(' ', $text, $excerpt_length + 1);
        if (count($words) > $excerpt_length) {
            array_pop($words);
            array_push($words, '[...]');
            $text = implode(' ', $words);
        }
    }
    return $text;
}

#remove_filter('get_the_excerpt', 'wp_trim_excerpt', 10);
#add_filter('get_the_excerpt', 'improved_trim_excerpt', 10);


/**
 * INCLUDE FEATURED IMAGE IN RSS
 *
 * @param $content
 * @return string
 */
function rss_post_thumbnail($content)
{
    global $post;
    $featured_image = get_the_post_thumbnail($post->ID, 'medium', array('class' => 'alignleft'));

    if ($featured_image) :
        return "<p>" . $featured_image . "</p>";
    else :
        echo "<p>" . get_first_image($post->ID, 'thumbnail') . "</p>";
    endif;

    return $content;
}

#add_filter('the_excerpt_rss', 'rss_post_thumbnail');
#add_filter('the_content_feed', 'rss_post_thumbnail');


/**
 * APPLY SHORT CODE AND AUTO EMBED FOR WIDGETS
 */
# add_filter('widget_text', array($wp_embed, 'run_shortcode'), 8);
# add_filter('widget_text', array($wp_embed, 'autoembed'), 8);


/**
 * Media library admin columns with attachment id
 *
 * @href http://wpsnipp.com/index.php/functions-php/media-library-admin-columns-with-attachment-id/
 */
add_filter('manage_media_columns', 'posts_columns_attachment_id', 1);
add_action('manage_media_custom_column', 'posts_custom_columns_attachment_id', 1, 2);
function posts_columns_attachment_id($defaults)
{
    $defaults['wps_post_attachments_id'] = __('ID');
    return $defaults;
}

function posts_custom_columns_attachment_id($column_name, $id)
{
    if ($column_name === 'wps_post_attachments_id') {
        echo $id;
    }
}


;

/**
 * Remove more Jump Link Hash
 *
 * @param $link
 * @return mixed
 */
function remove_more_jump_link($link)
{
    $offset = strpos($link, '#more-');
    if ($offset) {
        $end = strpos($link, '"', $offset);
    }
    if ($end) {
        $link = substr_replace($link, '', $offset, $end - $offset);
    }
    return $link;
}

add_filter('the_content_more_link', 'remove_more_jump_link');


/**
 * Add prev and next links to a numbered link list
 *
 * @param $args
 * @return array|string
 * @link http://wordpress.stackexchange.com/questions/39622/make-current-active-page-number-a-link-wp-link-pages
 */
function multi_page_nav($args = '')
{
    $defaults = array(
        'before' => '<p>' . __('Pages:'),
        'after' => '</p>',
        'link_before' => '', 'link_after' => '',
        'next_or_number' => 'number', 'nextpagelink' => __('Next page'),
        'previouspagelink' => __('Previous page'), 'pagelink' => '%',
        'current_before' => '<span>',
        'current_after' => '</span>',
        'echo' => 1
    );

    $r = wp_parse_args($args, $defaults);
    $r = apply_filters('wp_link_pages_args', $r);
    extract($r, EXTR_SKIP);

    global $page, $numpages, $multipage, $more, $pagenow;
    $output = '';
    if ($multipage) {
        if ('number' == $next_or_number) {
            $output .= $before;
            for ($i = 1; $i < ($numpages + 1); $i = $i + 1) {
                $j = str_replace('%', $i, $pagelink);
                $output .= ' ';
                if (($i != $page) || ((!$more) && ($page == 1))) {
                    $output .= _wp_link_page($i);
                    $output .= $link_before . $j . $link_after;
                } elseif ($i == $page) {
                    #$output .= '<a class="current-page" href="#">';
                    $output .= $current_before . $j . $current_after;
                }

                if (($i != $page) || ((!$more) && ($page == 1)))
                    $output .= '</a>';
            }
            $output .= $after;
        } else {
            if ($more) {
                $output .= $before;
                $i = $page - 1;
                if ($i && $more) {
                    $output .= _wp_link_page($i);
                    $output .= $link_before . $previouspagelink . $link_after . '</a>';
                }
                $i = $page + 1;
                if ($i <= $numpages && $more) {
                    $output .= _wp_link_page($i);
                    $output .= $link_before . $nextpagelink . $link_after . '</a>';
                }
                $output .= $after;
            }
        }
    }

    if ($echo)
        echo $output;

    return $output;
}


/**
 * Custom Next/Previous Page
 * Add prev and next links to a numbered link list
 * @param $args
 * @return array|string
 */
function wp_link_pages_args_prevnext_add($args)
{
    global $page, $numpages, $more, $pagenow;

    if (!$args['next_or_number'] == 'next_and_number')
        return $args; # exit early

    $args['next_or_number'] = 'number'; # keep numbering for the main part
    if (!$more)
        return $args; # exit early

    if ($page - 1) # there is a previous page
        $args['before'] .= _wp_link_page($page - 1)
            . $args['link_before'] . $args['previouspagelink'] . $args['link_after'] . '</a>';

    if ($page < $numpages) # there is a next page
        $args['after'] = _wp_link_page($page + 1)
            . $args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a>'
            . $args['after'];

    return $args;
}

add_filter('wp_link_pages_args', 'wp_link_pages_args_prevnext_add');


class description_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth, $args)
      {
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

           $class_names = $value = '';

           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. esc_attr( $class_names ) . '"';

           $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

           $prepend = '';
           $append = '';
           $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

           if($depth != 0)
           {
                     $description = $append = $prepend = "";
           }

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            $item_output .= $args->link_after;
            $item_output .= '</a>'.$description;
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }
}
?>