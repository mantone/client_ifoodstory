<?php
/****
 * functions/functions-media-galleries.php
 *
 * CUSTOM MEDIA GALLERIES INCORPORATED INTO OUR THEME FRAMEWORK
 */


/**
 *	LOAD MEDIA GALLERIES
 */
require_once(TEMPLATEPATH . '/functions/media-galleries/gallery-thefoundation.php');
require_once(TEMPLATEPATH . '/functions/media-galleries/gallery-featuredthumbnail.php');


?>